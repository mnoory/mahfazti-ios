//
//  ViewController.swift
//  Mahfazti
//
//  Created by zaid farouqi on 8/19/20.
//  Copyright © 2020 Umniah. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func goToAppStore(_ sender: Any) {
        
        if let url = URL(string: "itms-apps://apple.com/app/id1517825593") {
            UIApplication.shared.open(url)
        }
    }
    
}

